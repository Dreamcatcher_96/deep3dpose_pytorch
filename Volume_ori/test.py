import numpy as np
import time
from PIL import Image
from torchvision import transforms
import argparse
from data_loader import get_loader
import cv2
import torch
from hourglass import HourglassNet, Bottleneck
from network import VSPNetworkV2H

img = cv2.imread('/run/user/1000/gvfs/smb-share:server=192.168.1.101,share=share/zhangsiqi/ski.jpg')




# parser = argparse.ArgumentParser()
# parser.add_argument('--image_size', type=int, default=256)
# parser.add_argument('--stride', type=int, default=8)
# parser.add_argument('--sigma', type=float, default=7)
# parser.add_argument('--batch_size', type=float, default=1)
#
# config = parser.parse_args()
#
# loader = get_loader('../data', config, test_index=[1, 2, 3, 4, 5, 6], is_heatmap=True)
#
# for epoch in range(2):
#     for i_batch, data in enumerate(loader):
#         if epoch == 0:
#             print(i_batch)
#             continue
#         coord = [data[0][1]['key_points'][i]['coord'].numpy()[0, :] for i in range(14)]
#         img_raw = cv2.imread(data[0][1]['img_path'][0])
#         img = data[0][0][0, :, :, :]
#         img = (img.permute(1, 2, 0).numpy() + 0.5) * 256
#         img = img.astype(np.uint8)
#         for i in range(len(coord)):
#             img[int(coord[i][1]-1):int(coord[i][1]+1), int(coord[i][0]-1):int(coord[i][0]+1), :] = (0, 255, 0)
#             # cv2.circle(img, center=(coord[i][0], coord[i][1]), radius=1, color=(0, 255, 0))
#         cv2.imshow('res', img)
#         cv2.waitKey()
#
# print(config)

# checkpoint = torch.load('/media/alan/Disk/Downloads/drive-download-20180505T083109Z-001/checkpoint.pth.tar')
# checkpoint2 = torch.load(
#     '/home/alan/workspace/deep3dpose_pytorch/Volume_ori/results/D3P2V_Volume_2018_04_28_17_37_56/checkpoint_152000.pth.tar')

# img = np.zeros([1, 3, 256, 256])
# img = torch.from_numpy(img)
# img = torch.autograd.Variable(img).float()

# net = VSPNetworkV2H().cuda()
# model_dict = net.state_dict()
# base_model = torch.load('/media/alan/Disk/workspace/deep3dpose_pytorch/hourglass.pth.tar')
#
# base_model['state_dict'] = {k: v for k, v in base_model['state_dict'].items() if k in model_dict}
# model_dict.update(base_model['state_dict'])
#
# net.load_state_dict(model_dict)
# hourglass = HourglassNet(Bottleneck, num_stacks=2, num_blocks=1, num_classes=16).cuda()

# for i_batch, data in enumerate(loader):
#     # img = torch.cat([data[0][0], data[1][0]], dim=0)
#     img1 = torch.autograd.Variable(data[0][0].cuda())
#     img2 = torch.autograd.Variable(data[1][0].cuda())
#     output = net.forward(img1, img2)
#     heat1_gt = data[0][1]['heatmap'].numpy()
#     heat2_gt = data[1][1]['heatmap'].numpy()
#     img_show1 = cv2.resize(np.transpose(data[0][0].numpy(), (0, 2, 3, 1))[0], dsize=(64, 64))
#     img_show2 = cv2.resize(np.transpose(data[1][0].numpy(), (0, 2, 3, 1))[0], dsize=(64, 64))
#     cv2.imshow('img1', img_show1)
#     cv2.imshow('img2', img_show2)
#
#     heat1 = output[2][-1].data.cpu().numpy()
#     heat2 = output[3][-1].data.cpu().numpy()
#
#     for i in range(16):
#         # cv2.imshow('heat1_gt', heat1_gt[0, :, :, i])
#         # cv2.imshow('heat2_gt', heat2_gt[0, :, :, i])
#         cv2.imshow('heatmap1', heat1[0, i, :, :])
#         cv2.imshow('heatmap2', heat2[0, i, :, :])
#         cv2.waitKey()
# loss_heat_l = criterion.forward(heatmap_l, heatmap_l_gt)
# loss_heat_r = criterion.forward(heatmap_r, heatmap_r_gt)
# loss, conf_loss, loc_loss = VSPLoss.forward(output_classify, output_loc,
#                                             label_class.long(), label_loc.float())
# loss_all = loss_heat_l + loss_heat_r + loss
# hourglass = HourglassNet(Bottleneck, num_stacks=2, num_blocks=1, num_classes=14)
# res = hourglass.forward(img)

print('end')
