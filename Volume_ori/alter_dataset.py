import numpy as np
import os
import copy

points_index = [12, 11, 20, 21, 22, 14, 15, 16, 1, 2, 3, 5, 6, 7]
path = './data'

if not os.path.exists('./data/heatmap'):
    os.makedirs('./data/heatmap', exist_ok=True)

data_list = os.listdir(path)
data_list.sort()
data = []
for (i, item) in enumerate(data_list):
    if item.split('.')[-1] == 'npy':
        data.append(np.load(os.path.join(path, item)))

for i in range(len(data)):
    for j in range(len(data[i])):
        for k in range(len(data[i][j])):
            if len(data[i][j][k]) == 0:
                # print('%d %d %d' % (i, j, k))
                continue
            data[i][j][k]['key_points'] = \
                [data[i][j][k]['key_points'][index] for index in points_index]
            for i_data in range(len(data[i][j][k]['key_points'])):
                data[i][j][k]['key_points'][i_data]['id'] = i_data + 1
            print('Loading: %d/%d %d/%d %d/%d' % (i + 1, len(data), j + 1, len(data[i]),
                                                  k + 1, len(data[i][j])))

for i in range(len(data)):
    for j in range(len(data[i])):
        for k in range(len(data[i][j])):
            if len(data[i][j][k]) == 0:
                # print('%d %d %d' % (i, j, k))
                continue
            key_points = copy.deepcopy(data[i][j][k]['key_points'])
            new_key_points = {}
            for key in key_points[0].keys():
                new_key_points[key] = []
                for i_kpt in range(len(key_points)):
                    new_key_points[key].append(key_points[i_kpt][key])
