import torch
from collections import OrderedDict

base_model = torch.load('/media/alan/Disk/workspace/deep3dpose_pytorch/stack2hourglass.pth.tar')

state_dict = base_model['state_dict']

new_state_dict = OrderedDict({})

for i_key in state_dict.keys():
    key = 'hourglass' + i_key[len(i_key.split('.')[0]):]
    if key == 'hourglass.score.0.weight':
        continue
    elif key == 'hourglass.score.0.bias':
        continue
    elif key == 'hourglass.score_.0.weight':
        continue
    elif key == 'hourglass.score_.0.bias':
        continue
    elif key == 'hourglass.score.1.weight':
        continue
    elif key == 'hourglass.score.1.bias':
        continue
    else:
        new_state_dict[key] = state_dict[i_key]

torch.save({'state_dict': new_state_dict}, '/media/alan/Disk/workspace/deep3dpose_pytorch/hourglass.pth.tar')
