import module
import torch
import torch.nn as nn
from hourglass import HourglassNet, Bottleneck


class OpenposeBasedNet(nn.Module):
    def __init__(self, is_deconv=True):
        super(OpenposeBasedNet, self).__init__()
        self.is_deconv = is_deconv

        net_dict = self.build_model()

        self.base = module.make_layers(net_dict[0], 3)
        self.stage1_heat = module.make_layers(net_dict[1][0], 128)
        self.stage1_vec = module.make_layers(net_dict[1][1], 128)
        self.stage2_heat = module.make_layers(net_dict[2][0], 185)
        self.stage2_vec = module.make_layers(net_dict[2][1], 185)
        self.stage3_heat = module.make_layers(net_dict[3][0], 185)
        self.stage3_vec = module.make_layers(net_dict[3][1], 185)
        self.stage4_heat = module.make_layers(net_dict[4][0], 185)
        self.stage4_vec = module.make_layers(net_dict[4][1], 185)
        self.stage5_heat = module.make_layers(net_dict[5][0], 185)
        self.stage5_vec = module.make_layers(net_dict[5][1], 185)
        self.stage6_heat = module.make_layers(net_dict[6][0], 185)
        self.stage6_vec = module.make_layers(net_dict[6][1], 185)
        self.deconv = module.make_layers(net_dict[7], 256)

    def build_model(self, num_point=19, num_vector=19, num_stages=6):
        net_dict = []
        block0 = [{'conv1_1': ['conv2d', 64, 3, 1, 1, True]}, {'conv1_2': ['conv2d', 64, 3, 1, 1, True]},
                  {'pool1': ['pool2d', 2, 2, 0, 'Max']},
                  {'conv2_1': ['conv2d', 128, 3, 1, 1, True]}, {'conv2_2': ['conv2d', 128, 3, 1, 1, True]},
                  {'pool2': ['pool2d', 2, 2, 0, 'Max']},
                  {'conv3_1': ['conv2d', 256, 3, 1, 1, True]}, {'conv3_2': ['conv2d', 256, 3, 1, 1, True]},
                  {'conv3_3': ['conv2d', 256, 3, 1, 1, True]},
                  {'conv3_4': ['conv2d', 256, 3, 1, 1, True]}, {'pool3': ['pool2d', 2, 2, 0, 'Max']},
                  {'conv4_1': ['conv2d', 512, 3, 1, 1, True]}, {'conv4_2': ['conv2d', 512, 3, 1, 1, True]},
                  {'conv4_3_cpm': ['conv2d', 256, 3, 1, 1, True]},
                  {'conv4_4_cpm': ['conv2d', 128, 3, 1, 1, True]}]
        net_dict.append(block0)

        block1 = [[], []]
        in_vec = [0, 128, 128, 128, 128, 512, num_vector * 2]
        in_heat = [0, 128, 128, 128, 128, 512, num_point]
        for i in range(1, 6):
            if i < 4:
                block1[0].append({'conv{}_stage1_vec'.format(i): ['conv2d', in_vec[i + 1], 3, 1, 1, True]})
                block1[1].append({'conv{}_stage1_heat'.format(i): ['conv2d', in_heat[i + 1], 3, 1, 1, True]})
            elif i == 5:
                block1[0].append({'conv{}_stage1_vec'.format(i): ['conv2d', in_vec[i + 1], 1, 1, 0, False]})
                block1[1].append({'conv{}_stage1_heat'.format(i): ['conv2d', in_heat[i + 1], 1, 1, 0, False]})
            else:
                block1[0].append({'conv{}_stage1_vec'.format(i): ['conv2d', in_vec[i + 1], 1, 1, 0, True]})
                block1[1].append({'conv{}_stage1_heat'.format(i): ['conv2d', in_heat[i + 1], 1, 1, 0, True]})
        net_dict.append(block1)

        in_vec_1 = [0, 128 + num_point + num_vector * 2, 128, 128, 128, 128, 128, 128, num_vector * 2]
        in_heat_1 = [0, 128 + num_point + num_vector * 2, 128, 128, 128, 128, 128, 128, num_point]
        for j in range(2, num_stages + 1):
            blocks = [[], []]
            for i in range(1, 8):
                if i < 6:
                    blocks[0].append({'conv{}_stage{}_vec'.format(i, j): ['conv2d', in_vec_1[i + 1], 7, 1, 3, True]})
                    blocks[1].append({'conv{}_stage{}_heat'.format(i, j): ['conv2d', in_heat_1[i + 1], 7, 1, 3, True]})
                elif i == 7:
                    blocks[0].append({'conv{}_stage{}_vec'.format(i, j): ['conv2d', in_vec_1[i + 1], 1, 1, 0, False]})
                    blocks[1].append({'conv{}_stage{}_heat'.format(i, j): ['conv2d', in_heat_1[i + 1], 1, 1, 0, False]})
                else:
                    blocks[0].append({'conv{}_stage{}_vec'.format(i, j): ['conv2d', in_vec_1[i + 1], 1, 1, 0, True]})
                    blocks[1].append({'conv{}_stage{}_heat'.format(i, j): ['conv2d', in_heat_1[i + 1], 1, 1, 0, True]})
            if j == num_stages:
                blocks[0].pop()
                blocks[1].pop()
            net_dict.append(blocks)
        deconv_block = [{'feature_output': ['conv2d', 128, 3, 1, 1, True]},
                        {'feature_deconv1': ['deconv2d', 128, 4, 2, 1, True]},
                        {'feature_deconv2': ['deconv2d', 128, 4, 2, 1, True]},
                        {'feature_deconv3': ['deconv2d', 128, 4, 2, 1, True]}]
        net_dict.append(deconv_block)
        return net_dict

    def forward(self, x):
        out1 = self.base(x)

        out2_1 = self.stage1_heat(out1)
        out2_2 = self.stage1_vec(out1)
        out2 = torch.cat([out2_1, out2_2, out1], 1)

        out3_1 = self.stage2_heat(out2)
        out3_2 = self.stage2_vec(out2)
        out3 = torch.cat([out3_1, out3_2, out2], 1)

        out4_1 = self.stage3_heat(out3)
        out4_2 = self.stage3_vec(out3)
        out4 = torch.cat([out4_1, out4_2, out3], 1)

        out5_1 = self.stage4_heat(out4)
        out5_2 = self.stage4_vec(out4)
        out5 = torch.cat([out5_1, out5_2, out4], 1)

        out6_1 = self.stage5_heat(out5)
        out6_2 = self.stage5_vec(out5)
        out6 = torch.cat([out6_1, out6_2, out5], 1)

        out7_1 = self.stage6_heat(out6)
        out7_2 = self.stage6_vec(out6)
        out7 = torch.cat([out7_1, out7_2, out6], 1)

        deconv_out = self.deconv(out7)
        return deconv_out


class VSPNetwork(nn.Module):
    def __init__(self, concat_method='Add'):
        super(VSPNetwork, self).__init__()

        self.concat_method = concat_method

        self.left_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.left_r1 = module.ResModule(64, 128)
        self.left_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.left_r2 = module.ResModule(128, 128)
        self.left_r3 = module.ResModule(128, 256)
        self.left_perceptron = module.Hourglass(256, 256, layers=4)
        self.left_r4 = module.ResModule(256, 256)
        self.left_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        self.right_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.right_r1 = module.ResModule(64, 128)
        self.right_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.right_r2 = module.ResModule(128, 128)
        self.right_r3 = module.ResModule(128, 256)
        self.right_perceptron = module.Hourglass(256, 256, layers=4)
        self.right_r4 = module.ResModule(256, 256)
        self.right_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        self.left_stage1 = module.ResModule(256, 256)
        self.left_stage2 = module.ResModule(256, 256)
        self.left_stage3 = module.ResModule(256, 256)
        self.left_stage4 = module.ResModule(256, 256)

        self.right_stage1 = module.ResModule(256, 256)
        self.right_stage2 = module.ResModule(256, 256)
        self.right_stage3 = module.ResModule(256, 256)
        self.right_stage4 = module.ResModule(256, 256)

        self.conv1 = module.Conv2d(256 + 14, 256, kernel_size=3, padding=1, relu=True, bn=True)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.r1 = module.ResModule(256, 256)
        self.r2 = module.ResModule(256, 256)
        self.r3 = module.ResModule(256, 256)
        self.r4 = module.ResModule(256, 256)

        self.fc = nn.Linear(4096, 42)

    def forward(self, x1, x2):
        x1 = self.left_conv1.forward(x1)
        x1 = self.left_r1.forward(x1)
        x1 = self.left_maxpool(x1)
        x1 = self.left_r2.forward(x1)
        x1 = self.left_r3.forward(x1)
        x1, inter_out_left = self.left_perceptron.forward(x1)
        x1 = self.left_r4.forward(x1)
        x1 = self.left_conv2.forward(x1)

        x1_stage1 = self.left_stage1.forward(inter_out_left[0])
        x1_stage2 = self.left_stage2.forward(inter_out_left[1])
        x1_stage3 = self.left_stage3.forward(inter_out_left[2])
        x1_stage4 = self.left_stage4.forward(inter_out_left[3])

        x2 = self.right_conv1.forward(x2)
        x2 = self.right_r1.forward(x2)
        x2 = self.right_maxpool(x2)
        x2 = self.right_r2.forward(x2)
        x2 = self.right_r3.forward(x2)
        x2, inter_out_right = self.left_perceptron.forward(x2)
        x2 = self.right_r4.forward(x2)
        x2 = self.right_conv2.forward(x2)

        x2_stage1 = self.right_stage1.forward(inter_out_right[0])
        x2_stage2 = self.right_stage2.forward(inter_out_right[1])
        x2_stage3 = self.right_stage3.forward(inter_out_right[2])
        x2_stage4 = self.right_stage4.forward(inter_out_right[3])

        if self.concat_method == 'Add':
            x_heatmap = x1 + x2
            x_stage1 = x1_stage1 + x2_stage1
        x_out = torch.cat([x_heatmap, x_stage1], dim=1)
        x_out = self.conv1.forward(x_out)
        x_out = self.maxpool(x_out)
        x_out = self.r1.forward(x_out)
        if self.concat_method == 'Add':
            x_stage2 = x1_stage2 + x2_stage2 + x_out
        # x_out = torch.cat([x_out, x_stage2], dim=2)
        x_out = self.maxpool(x_stage2)
        x_out = self.r2.forward(x_out)
        if self.concat_method == 'Add':
            x_stage3 = x1_stage3 + x2_stage3 + x_out
        # x_out = torch.cat([x_out, x_stage3], dim=2)
        x_out = self.maxpool(x_stage3)
        x_out = self.r3.forward(x_out)
        if self.concat_method == 'Add':
            x_stage4 = x1_stage4 + x2_stage4 + x_out
        # x_out = torch.cat([x_out, x_stage4], dim=2)
        x_out = self.maxpool(x_stage4)
        x_out = self.r4.forward(x_out)
        x_out = x_out.view(x_out.size(0), -1)
        x_out = self.fc(x_out)

        return x_out, x1, x2


class VSPNetworkSub(nn.Module):
    def __init__(self, concat_method='Add'):
        super(VSPNetworkSub, self).__init__()

        self.concat_method = concat_method

        self.left_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.left_r1 = module.ResModule(64, 128)
        self.left_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.left_r2 = module.ResModule(128, 128)
        self.left_r3 = module.ResModule(128, 256)
        self.left_perceptron = module.Hourglass(256, 256, layers=4)
        self.left_r4 = module.ResModule(256, 256)
        self.left_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        self.right_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.right_r1 = module.ResModule(64, 128)
        self.right_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.right_r2 = module.ResModule(128, 128)
        self.right_r3 = module.ResModule(128, 256)
        self.right_perceptron = module.Hourglass(256, 256, layers=4)
        self.right_r4 = module.ResModule(256, 256)
        self.right_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        # self.left_stage1 = module.ResModule(256, 256)
        # self.left_stage2 = module.ResModule(256, 256)
        # self.left_stage3 = module.ResModule(256, 256)
        # self.left_stage4 = module.ResModule(256, 256)
        #
        # self.right_stage1 = module.ResModule(256, 256)
        # self.right_stage2 = module.ResModule(256, 256)
        # self.right_stage3 = module.ResModule(256, 256)
        # self.right_stage4 = module.ResModule(256, 256)
        #
        # self.conv1 = module.Conv2d(256 + 14, 256, kernel_size=3, padding=1, relu=True, bn=True)
        # self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        # self.r1 = module.ResModule(256, 256)
        # self.r2 = module.ResModule(256, 256)
        # self.r3 = module.ResModule(256, 256)
        # self.r4 = module.ResModule(256, 256)

        # self.fc = nn.Linear(4096, 42)

    def forward(self, x1, x2):
        x1 = self.left_conv1.forward(x1)
        x1 = self.left_r1.forward(x1)
        x1 = self.left_maxpool(x1)
        x1 = self.left_r2.forward(x1)
        x1 = self.left_r3.forward(x1)
        x1, inter_out_left = self.left_perceptron.forward(x1)
        x1 = self.left_r4.forward(x1)
        x1 = self.left_conv2.forward(x1)

        # x1_stage1 = self.left_stage1.forward(inter_out_left[0])
        # x1_stage2 = self.left_stage2.forward(inter_out_left[1])
        # x1_stage3 = self.left_stage3.forward(inter_out_left[2])
        # x1_stage4 = self.left_stage4.forward(inter_out_left[3])

        x2 = self.right_conv1.forward(x2)
        x2 = self.right_r1.forward(x2)
        x2 = self.right_maxpool(x2)
        x2 = self.right_r2.forward(x2)
        x2 = self.right_r3.forward(x2)
        x2, inter_out_right = self.left_perceptron.forward(x2)
        x2 = self.right_r4.forward(x2)
        x2 = self.right_conv2.forward(x2)

        # x2_stage1 = self.right_stage1.forward(inter_out_right[0])
        # x2_stage2 = self.right_stage2.forward(inter_out_right[1])
        # x2_stage3 = self.right_stage3.forward(inter_out_right[2])
        # x2_stage4 = self.right_stage4.forward(inter_out_right[3])
        #
        # if self.concat_method == 'Add':
        #     x_heatmap = x1 + x2
        #     x_stage1 = x1_stage1 + x2_stage1
        # x_out = torch.cat([x_heatmap, x_stage1], dim=1)
        # x_out = self.conv1.forward(x_out)
        # x_out = self.maxpool(x_out)
        # x_out = self.r1.forward(x_out)
        # if self.concat_method == 'Add':
        #     x_stage2 = x1_stage2 + x2_stage2 + x_out
        # # x_out = torch.cat([x_out, x_stage2], dim=2)
        # x_out = self.maxpool(x_stage2)
        # x_out = self.r2.forward(x_out)
        # if self.concat_method == 'Add':
        #     x_stage3 = x1_stage3 + x2_stage3 + x_out
        # # x_out = torch.cat([x_out, x_stage3], dim=2)
        # x_out = self.maxpool(x_stage3)
        # x_out = self.r3.forward(x_out)
        # if self.concat_method == 'Add':
        #     x_stage4 = x1_stage4 + x2_stage4 + x_out
        # # x_out = torch.cat([x_out, x_stage4], dim=2)
        # x_out = self.maxpool(x_stage4)
        # x_out = self.r4.forward(x_out)
        # x_out = x_out.view(x_out.size(0), -1)
        # x_out = self.fc(x_out)

        return x1, x2


class VSPNetworkV(nn.Module):
    def __init__(self, concat_method='Add'):
        super(VSPNetworkV, self).__init__()

        self.concat_method = concat_method

        self.left_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.left_r1 = module.ResModule(64, 128)
        self.left_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.left_r2 = module.ResModule(128, 128)
        self.left_r3 = module.ResModule(128, 256)
        self.left_perceptron = module.Hourglass(256, 256, layers=4)
        self.left_r4 = module.ResModule(256, 256)
        self.left_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        self.right_conv1 = module.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, relu=True, bn=True)
        self.right_r1 = module.ResModule(64, 128)
        self.right_maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.right_r2 = module.ResModule(128, 128)
        self.right_r3 = module.ResModule(128, 256)
        self.right_perceptron = module.Hourglass(256, 256, layers=4)
        self.right_r4 = module.ResModule(256, 256)
        self.right_conv2 = module.Conv2d(256, 14, kernel_size=1, relu=False, bn=False)

        self.left_stage1 = module.ResModule(256, 256)
        self.left_stage1_conv = module.Conv3d(4, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.left_stage2 = module.ResModule(256, 256)
        self.left_stage2_conv = module.Conv3d(8, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.left_stage3 = module.ResModule(256, 256)
        self.left_stage3_conv = module.Conv3d(16, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.left_stage4 = module.ResModule(256, 256)
        self.left_stage4_conv = module.Conv3d(32, 32, kernel_size=3, padding=1, relu=True, bn=True)

        self.right_stage1 = module.ResModule(256, 256)
        self.right_stage1_conv = module.Conv3d(4, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.right_stage2 = module.ResModule(256, 256)
        self.right_stage2_conv = module.Conv3d(8, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.right_stage3 = module.ResModule(256, 256)
        self.right_stage3_conv = module.Conv3d(16, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.right_stage4 = module.ResModule(256, 256)
        self.right_stage4_conv = module.Conv3d(32, 32, kernel_size=3, padding=1, relu=True, bn=True)

        self.conv1 = module.Conv2d(14, 256, kernel_size=3, padding=1, relu=True, bn=True)
        self.conv2 = module.Conv3d(32 + 4, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.maxpool = nn.MaxPool3d(kernel_size=2, stride=2)
        self.r1 = module.ResModule3D(32, 32)
        self.r2 = module.ResModule3D(32, 32)
        self.r3 = module.ResModule3D(32, 32)
        self.conv3 = module.Conv3d(32, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.r4 = module.ResModule3D(32, 32)

        self.classify = module.Conv3d(32, 14, kernel_size=3, padding=1, relu=False, bn=False)
        self.loc = module.Conv3d(32, 14 * 3, kernel_size=3, padding=1, relu=False, bn=False)


        # self.downsample1 = module.ResModule(256, 128)
        # self.downsample2 = module.ResModule(128, 4)

        # self.fc = nn.Linear(4096, 42)

    def forward(self, x1, x2):
        x1 = self.left_conv1.forward(x1)
        x1 = self.left_r1.forward(x1)
        x1 = self.left_maxpool(x1)
        x1 = self.left_r2.forward(x1)
        x1 = self.left_r3.forward(x1)
        x1, inter_out_left = self.left_perceptron.forward(x1)
        x1 = self.left_r4.forward(x1)
        x1 = self.left_conv2.forward(x1)

        x1_stage1 = self.left_stage1.forward(inter_out_left[0])
        x1_stage1 = x1_stage1.view(x1_stage1.size(0), -1, x1_stage1.size(2), x1_stage1.size(2), x1_stage1.size(2))
        x1_stage1 = self.left_stage1_conv.forward(x1_stage1)

        x1_stage2 = self.left_stage2.forward(inter_out_left[1])
        x1_stage2 = x1_stage2.view(x1_stage2.size(0), -1, x1_stage2.size(2), x1_stage2.size(2), x1_stage2.size(2))
        x1_stage2 = self.left_stage2_conv.forward(x1_stage2)

        x1_stage3 = self.left_stage3.forward(inter_out_left[2])
        x1_stage3 = x1_stage3.view(x1_stage3.size(0), -1, x1_stage3.size(2), x1_stage3.size(2), x1_stage3.size(2))
        x1_stage3 = self.left_stage3_conv.forward(x1_stage3)

        x1_stage4 = self.left_stage4.forward(inter_out_left[3])
        x1_stage4 = x1_stage4.view(x1_stage4.size(0), -1, x1_stage4.size(2), x1_stage4.size(2), x1_stage4.size(2))
        x1_stage4 = self.left_stage4_conv.forward(x1_stage4)

        x2 = self.right_conv1.forward(x2)
        x2 = self.right_r1.forward(x2)
        x2 = self.right_maxpool(x2)
        x2 = self.right_r2.forward(x2)
        x2 = self.right_r3.forward(x2)
        x2, inter_out_right = self.right_perceptron.forward(x2)
        x2 = self.right_r4.forward(x2)
        x2 = self.right_conv2.forward(x2)

        x2_stage1 = self.right_stage1.forward(inter_out_right[0])
        x2_stage1 = x2_stage1.view(x2_stage1.size(0), -1, x2_stage1.size(2), x2_stage1.size(2), x2_stage1.size(2))
        x2_stage1 = self.right_stage1_conv.forward(x2_stage1)

        x2_stage2 = self.right_stage2.forward(inter_out_right[1])
        x2_stage2 = x2_stage2.view(x2_stage2.size(0), -1, x2_stage2.size(2), x2_stage2.size(2), x2_stage2.size(2))
        x2_stage2 = self.right_stage2_conv.forward(x2_stage2)

        x2_stage3 = self.right_stage3.forward(inter_out_right[2])
        x2_stage3 = x2_stage3.view(x2_stage3.size(0), -1, x2_stage3.size(2), x2_stage3.size(2), x2_stage3.size(2))
        x2_stage3 = self.right_stage3_conv.forward(x2_stage3)

        x2_stage4 = self.right_stage4.forward(inter_out_right[3])
        x2_stage4 = x2_stage4.view(x2_stage4.size(0), -1, x2_stage4.size(2), x2_stage4.size(2), x2_stage4.size(2))
        x2_stage4 = self.right_stage4_conv.forward(x2_stage4)

        if self.concat_method == 'Add':
            x_heatmap = x1 + x2
            x_stage1 = x1_stage1 + x2_stage1
        x_heatmap = self.conv1.forward(x_heatmap)
        x_heatmap = x_heatmap.view(x_heatmap.size(0), -1, x_heatmap.size(2), x_heatmap.size(2), x_heatmap.size(2))
        x_out = torch.cat([x_heatmap, x_stage1], dim=1)
        x_out = self.conv2.forward(x_out)
        x_out = self.maxpool(x_out)
        x_out = self.r1.forward(x_out)
        if self.concat_method == 'Add':
            x_stage2 = x1_stage2 + x2_stage2 + x_out
        # x_out = torch.cat([x_out, x_stage2], dim=2)
        x_out = self.maxpool(x_stage2)
        x_out = self.r2.forward(x_out)
        if self.concat_method == 'Add':
            x_stage3 = x1_stage3 + x2_stage3 + x_out
        # x_out = torch.cat([x_out, x_stage3], dim=2)
        x_out = self.maxpool(x_stage3)
        x_out = self.r3.forward(x_out)
        if self.concat_method == 'Add':
            x_stage4 = x1_stage4 + x2_stage4 + x_out
        # x_out = torch.cat([x_out, x_stage4], dim=2)
        # x_out = self.maxpool(x_stage4)
        x_out = self.conv3.forward(x_stage4)
        x_out = self.r4.forward(x_out)

        classify = self.classify.forward(x_out)
        classify = classify.view(classify.size(0), 14, -1)
        loc = self.loc.forward(x_out)
        loc = loc.view(loc.size(0), 14, 3, -1)
        # loc_x = self.x_loc.forward(x_out)
        # loc_x = loc_x.view(loc_x.size(0), 14, -1)
        # loc_y = self.y_loc.forward(x_out)
        # loc_y = loc_y.view(loc_y.size(0), 14, -1)
        # loc_z = self.z_loc.forward(x_out)
        # loc_z = loc_z.view(loc_z.size(0), 14, -1)

        # x_out = x_out.view(x_out.size(0), -1)
        # x_out = self.fc(x_out)

        # x_out = self.downsample1.forward(x_out)
        # x_out = self.downsample2.forward(x_out)

        return classify, loc, x1, x2


class VSPNetworkV2H(nn.Module):
    def __init__(self, concat_method='Concat'):
        super(VSPNetworkV2H, self).__init__()

        self.concat_method = concat_method

        self.hourglass = HourglassNet(Bottleneck, num_stacks=2, num_blocks=1, num_classes=14)

        self.stage1 = module.ResModule(256*2+14*2, 256)
        # self.stage1_conv = module.Conv3d(4, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.stage2 = module.ResModule(256*3, 256)
        # self.stage2_conv = module.Conv3d(8+32, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.stage3 = module.ResModule(256*3, 256)
        # self.stage3_conv = module.Conv3d(16+32, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.stage4 = module.ResModule(256*3, 256)
        # self.stage4_conv = module.Conv3d(32+32, 32, kernel_size=3, padding=1, relu=True, bn=True)

        # self.conv1 = module.Conv2d(14, 256, kernel_size=3, padding=1, relu=True, bn=True)
        # self.conv2 = module.Conv3d(32 + 4, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.r1 = module.ResModule(256, 256)
        self.r2 = module.ResModule(256, 256)
        self.r3 = module.ResModule(256, 256)
        # self.conv3 = module.Conv3d(32, 32, kernel_size=3, padding=1, relu=True, bn=True)
        self.r4 = module.ResModule(256, 256)

        self.classify = module.Conv2d(256, 14, kernel_size=3, padding=1, relu=False, bn=False)
        self.loc = module.Conv2d(256, 14 * 3, kernel_size=3, padding=1, relu=False, bn=False)
        self.norm = module.L2Norm(3, 64, scale=0.5)

    def forward(self, x1, x2):
        x1, x1_stages = self.hourglass.forward(x1)
        x2, x2_stages = self.hourglass.forward(x2)

        if self.concat_method == 'Concat':
            x_stage1 = torch.cat([x1[-1], x2[-1], x1_stages[1][0], x2_stages[1][0]], dim=1)
        x_stage1 = self.stage1.forward(x_stage1)
        x_out = self.maxpool(x_stage1)
        x_out = self.r1.forward(x_out)

        if self.concat_method == 'Concat':
            x_stage2 = torch.cat([x_out, x1_stages[1][1], x2_stages[1][1]], dim=1)
        x_stage2 = self.stage2.forward(x_stage2)
        x_out = self.maxpool(x_stage2)
        x_out = self.r2.forward(x_out)

        if self.concat_method == 'Concat':
            x_stage3 = torch.cat([x_out, x1_stages[1][2], x2_stages[1][2]], dim=1)
        x_stage3 = self.stage3.forward(x_stage3)
        x_out = self.maxpool(x_stage3)
        x_out = self.r3.forward(x_out)

        if self.concat_method == 'Concat':
            x_stage4 = torch.cat([x_out, x1_stages[1][3], x2_stages[1][3]], dim=1)
        x_stage4 = self.stage4.forward(x_stage4)
        # x_out = self.maxpool(x_stage4)
        x_out = self.r4.forward(x_stage4)

        classify = self.classify.forward(x_out)
        classify = classify.view(classify.size(0), 14, -1)
        loc = self.loc.forward(x_out)
        loc = loc.view(loc.size(0), 14, 3, -1)
        loc = self.norm.forward(loc)

        return classify, loc, x1, x2
