import argparse
import os
import numpy as np
from data_loader import get_loader

# parser = argparse.ArgumentParser()
# parser.add_argument('--image_size', type=int, default=256)
# parser.add_argument('--stride', type=int, default=4)
# parser.add_argument('--sigma', type=float, default=7)
# parser.add_argument('--max_iter', type=int, default=600)
# parser.add_argument('--batch_size', type=int, default=12)
# parser.add_argument('--test_size', type=int, default=100)

# config = parser.parse_args()

# train_data = get_loader('./data', config, test_index=(), is_heatmap=False, shuffle=False)
# test_data = get_loader('./data', config, phase='Test', is_heatmap=False, shuffle=False)

points_index = [12, 11, 20, 21, 22, 14, 15, 16, 1, 2, 3, 5, 6, 7]

data = []
data_list = os.listdir('./data')
data_list.sort()
for (i, item) in enumerate(data_list):
    if item.split('.')[-1] == 'npy':
        data.append(np.load(os.path.join('./data', item)))
        print((i, item))

for i in range(len(data)):
    for j in range(len(data[i])):
        for k in range(len(data[i][j])):
            if len(data[i][j][k]) == 0:
                continue
            data[i][j][k]['key_points'] = \
                [data[i][j][k]['key_points'][index] for index in points_index]
            for i_data in range(len(data[i][j][k]['key_points'])):
                data[i][j][k]['key_points'][i_data]['id'] = i_data + 1
            print('Loading: %d/%d %d/%d %d/%d' % (i + 1, len(data), j + 1, len(data[i]),
                                                  k + 1, len(data[i][j])))
all_world_box = []
for i in range(len(data)):
    for j in range(len(data[i])):
        # for k in range(len(data[i][j])):
        single_world_points = []
        for k in range(len(data[i][j][1]['key_points'])):
            single_world_points.append(data[i][j][1]['key_points'][k]['world_coord'])
        single_world_points = np.asarray(single_world_points)
        max_xyz = np.max(single_world_points, axis=0)
        min_xyz = np.min(single_world_points, axis=0)
        box = (max_xyz + (max_xyz - min_xyz) * 0.05) - (min_xyz - (max_xyz - min_xyz) * 0.05)
        min_xyz = min_xyz - (max_xyz - min_xyz) * 0.05
        world_norm = (single_world_points - min_xyz) / box
        volume_class = np.zeros([4, 4, 4])
        volume_loc_x = np.zeros([4, 4, 4])
        volume_loc_y = np.zeros([4, 4, 4])
        volume_loc_z = np.zeros([4, 4, 4])

        for i_joint in range(len(world_norm)):
            x = int(np.floor(world_norm[i_joint, 0] / 0.25))
            y = int(np.floor(world_norm[i_joint, 1] / 0.25))
            z = int(np.floor(world_norm[i_joint, 2] / 0.25))
            x_loc = world_norm[i_joint, 0] - x * 0.25
            y_loc = world_norm[i_joint, 1] - y * 0.25
            z_loc = world_norm[i_joint, 2] - z * 0.25
            volume_class[x, y, z] = i_joint + 1
            volume_loc_x[x, y, z] = x_loc
            volume_loc_y[x, y, z] = y_loc
            volume_loc_z[x, y, z] = z_loc
        all_world_box.append(box)

all_world_box = np.asarray(all_world_box)

