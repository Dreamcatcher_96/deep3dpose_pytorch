import argparse
import numpy as np
import torch
import cv2
import matplotlib

matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from network import VSPNetworkV2H
from data_loader import get_loader
import time
from util import decode_volume

parser = argparse.ArgumentParser()
parser.add_argument('--image_size', type=int, default=256)
parser.add_argument('--stride', type=int, default=4)
parser.add_argument('--sigma', type=float, default=7)
parser.add_argument('--max_iter', type=int, default=600)
parser.add_argument('--batch_size', type=int, default=1)
config = parser.parse_args()

# train_data = get_loader('./data', config, is_heatmap=True, shuffle=False)
test_data = get_loader('../data', config, phase='Test', is_heatmap=True, is_volume=True, shuffle=False)

checkpoint = torch.load(
    '/home/alan/workspace/deep3dpose_pytorch/Volume_ori/results/D3P2V_Volume_2018_05_11_15_23_58/checkpoint_20000.pth.tar',
    map_location=lambda storage, loc: storage)

pairs_index = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 8], [8, 9], [9, 10], [1, 11], [11, 12],
               [12, 13]]

all_result = []
all_result_per_joint = []
all_conf_acc = []
all_loc_err = []
all_time = []
with torch.cuda.device(0):
    net = VSPNetworkV2H().cuda()
    net.load_state_dict(checkpoint['state_dict'])
    # net.eval()
    for i_batch, data in enumerate(test_data):
        if i_batch == 12:
            print('debug')
        # if i_batch == 100:
        #     break
        img1 = torch.autograd.Variable(data[0][0].cuda())
        img2 = torch.autograd.Variable(data[1][0].cuda())

        # label = np.reshape(data[0][1]['world_coord_offset'].view(data[0][1]['world_coord_offset'].shape[0], -1).numpy(),
        #                    [14, 3])
        tic = time.time()
        output = net.forward(img1, img2)
        toc = time.time()
        forward_time = toc - tic
        all_time.append(forward_time)
        print(forward_time)

        # img_show1 = cv2.imread(data[0][1]['img_path'][0])
        # img_show2 = cv2.imread(data[1][1]['img_path'][0])
        # cv2.imshow('img1', img_show1)
        # cv2.imshow('img2', img_show2)
        #
        # heat1_gt = data[0][1]['heatmap'].numpy()
        # heat2_gt = data[1][1]['heatmap'].numpy()
        # # cv2.imshow('heat1_gt', heat1_gt)
        # # cv2.imshow('heat2_gt', heat2_gt)
        #
        # heat1 = output[2][-1].data.cpu().numpy()
        # heat2 = output[3][-1].data.cpu().numpy()
        #
        # for i in range(14):
        #     cv2.imshow('heat1_gt', heat1_gt[0, :, :, i])
        #     cv2.imshow('heat2_gt', heat2_gt[0, :, :, i])
        #     cv2.imshow('heatmap1', heat1[0, i, :, :])
        #     cv2.imshow('heatmap2', heat2[0, i, :, :])
        #     cv2.waitKey()

        dims_tst = data[0][1]['dim'].numpy()
        bbox_3d_tst = [data[0][1]['3dbox_coord'][0].numpy(), data[0][1]['3dbox_coord'][1].numpy()]
        world_coord_gt_tst = data[0][1]['world_coord'].numpy()

        conf_tst = torch.nn.functional.softmax(output[0], dim=2).data.cpu().numpy()
        conf_tst = np.argmax(conf_tst, axis=2)
        conf_tst_gt = data[0][1]['volume_class'].numpy()

        conf_acc_tst = np.sum(conf_tst == conf_tst_gt) / conf_tst.size * 100
        all_conf_acc.append(conf_acc_tst)

        loc_tst_gt = data[0][1]['volume_loc'].numpy()
        # output_location_x = output_loc_x.data.cpu().numpy()
        # output_location_y = output_loc_y.data.cpu().numpy()
        # output_location_z = output_loc_z.data.cpu().numpy()
        loc_tst = []
        for i_joint in range(14):
            loc_tst_single = []
            for ii_batch in range(conf_tst.shape[0]):
                loc_tst_single.append(
                    output[1][ii_batch, i_joint, :, conf_tst[ii_batch, i_joint]].contiguous().view(1, -1))
            loc_tst.append(
                torch.cat(loc_tst_single, dim=0).view(len(loc_tst_single), -1, 3))
        loc_tst = torch.cat(loc_tst, dim=1).data.cpu().numpy()

        loc_err_tst = []
        for ii in range(loc_tst.shape[0]):
            single_tst_error = []
            for iii in range(int(loc_tst.shape[1])):
                single_tst_error.append(
                    np.linalg.norm((loc_tst[ii, iii, :] - loc_tst_gt[ii, iii, :]) * dims_tst[ii, :]))
            loc_err_tst.append(np.mean(single_tst_error))
        all_loc_err.append(np.mean(loc_err_tst))

        # if conf_acc_tst >= 95 and np.mean(loc_err_tst) < 0.08:
        #     print('debug')

        # if conf_acc_tst <= 10:
        #     img = cv2.imread(data[0][1]['img_path'][0])
        #     cv2.imshow('debug', img)
        #     cv2.waitKey()
        loc_tst_real = loc_tst * dims_tst[0, :]
        loc_tst = decode_volume(conf_tst, loc_tst, dims_tst, bbox_3d_tst)

        tst_error = []
        for ii in range(loc_tst.shape[0]):
            single_tst_error = []
            for iii in range(int(loc_tst.shape[1])):
                single_tst_error.append(
                    np.linalg.norm(loc_tst[ii, iii, :] - world_coord_gt_tst[ii, iii, :]))
            tst_error.append(np.mean(single_tst_error))
            all_result_per_joint.append(single_tst_error)
        all_result.append(np.mean(tst_error))

        # fig = plt.figure()
        # # ax = fig.add_subplot(111, projection='3d')
        # ax = Axes3D(fig)
        # ax.scatter(xs=loc_tst[0, :, 0], ys=loc_tst[0, :, 1], zs=loc_tst[0, :, 2])
        #
        # for ii in range(len(pairs_index)):
        #     # print(ii)
        #     # print([pairs_index[ii][0], pairs_index[ii][1]])
        #     ax.plot([loc_tst[0, pairs_index[ii][0], 0], loc_tst[0, pairs_index[ii][1], 0]],
        #             [loc_tst[0, pairs_index[ii][0], 1], loc_tst[0, pairs_index[ii][1], 1]],
        #             zs=[loc_tst[0, pairs_index[ii][0], 2], loc_tst[0, pairs_index[ii][1], 2]], color='green')
        #
        # ax.scatter(xs=world_coord_gt_tst[0, :, 0], ys=world_coord_gt_tst[0, :, 1], zs=world_coord_gt_tst[0, :, 2])
        #
        # for ii in range(len(pairs_index)):
        #     # print(ii)
        #     # print([pairs_index[ii][0], pairs_index[ii][1]])
        #     ax.plot([world_coord_gt_tst[0, pairs_index[ii][0], 0], world_coord_gt_tst[0, pairs_index[ii][1], 0]],
        #             [world_coord_gt_tst[0, pairs_index[ii][0], 1], world_coord_gt_tst[0, pairs_index[ii][1], 1]],
        #             zs=[world_coord_gt_tst[0, pairs_index[ii][0], 2], world_coord_gt_tst[0, pairs_index[ii][1], 2]], color='red')
        #
        # plt.axis('equal')
        for i_keypoint in range(14):
            print('joint_%d %f %f %f %f %f %f %f %f %f' % (i_keypoint,
                                                           loc_tst_real[0, i_keypoint, 0],
                                                           loc_tst_real[0, i_keypoint, 1],
                                                           loc_tst_real[0, i_keypoint, 2],
                                                           loc_tst[0, i_keypoint, 0],
                                                           loc_tst[0, i_keypoint, 1],
                                                           loc_tst[0, i_keypoint, 2],
                                                           world_coord_gt_tst[0, i_keypoint, 0],
                                                           world_coord_gt_tst[0, i_keypoint, 1],
                                                           world_coord_gt_tst[0, i_keypoint, 2]))
        print('%d/%d test_err:%.5f conf_acc:%.2f loc_err:%.5f dim: %f %f' % (
            i_batch, len(test_data), np.mean(tst_error), conf_acc_tst, np.mean(loc_err_tst), dims_tst[0, 0],
            dims_tst[0, 0] * 0.25))
        # plt.show()
        # time.sleep(0.1)
        # plt.close()

        # cv2.waitKey()

print(np.mean(all_result))
all_result_per_joint = np.asarray(all_result_per_joint)
all_result_per_joint = np.mean(all_result_per_joint, axis=1)
print(all_result_per_joint)
print(np.mean(all_conf_acc))
print(np.mean(all_loc_err))
print('end')
