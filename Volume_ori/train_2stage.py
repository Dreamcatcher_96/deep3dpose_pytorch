import torch
from network import OpenposeBasedNet
from tensorboardX import SummaryWriter
import numpy as np
from network import VSPNetwork
from network import VSPNetworkSub
from data_loader import get_loader
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument('--image_size', type=int, default=256)
parser.add_argument('--stride', type=int, default=8)
parser.add_argument('--sigma', type=float, default=7)
parser.add_argument('--max_iter', type=int, default=200)
parser.add_argument('--batch_size', type=int, default=12)

config = parser.parse_args()

writer = SummaryWriter()

train_data = get_loader('./data', config)
test_data = get_loader('./data', config, phase='Test')

with torch.cuda.device(2):
    sub_net = VSPNetworkSub().cuda()
    # criterion = module.L2Loss()
    criterion = torch.nn.MSELoss()

    lr = 0.0001
    optimizer = torch.optim.Adam(sub_net.parameters(), lr=lr)

    cnt = 0
    for i_epoch in range(config.max_iter):
        tic = time.time()
        for i_batch, data in enumerate(train_data):
            # img = torch.cat([data[0][0], data[1][0]], dim=0)
            img1 = torch.autograd.Variable(data[0][0].cuda())
            img2 = torch.autograd.Variable(data[1][0].cuda())
            toc = time.time()
            data_time = toc - tic

            tic = time.time()
            output = sub_net.forward(img1, img2)
            toc = time.time()
            forward_time = toc - tic

            label = np.asarray([data[0][1]['key_points'][i]['world_coord'].numpy() for i in range(14)])
            label = np.transpose(label, [1, 0, 2])
            label = torch.autograd.Variable(
                torch.from_numpy(np.reshape(label, [label.shape[0], label.shape[1] * label.shape[2]])).cuda())

            optimizer.zero_grad()

            tic = time.time()
            loss = criterion.forward(output, label)
            toc = time.time()
            loss_time = toc - tic

            tic = time.time()
            loss.backward()
            toc = time.time()
            backward_time = toc - tic

            tic = time.time()
            optimizer.step()
            toc = time.time()
            optim_time = toc - tic

            cnt += 1

            output = output.data.cpu().numpy()
            label = label.data.cpu().numpy()
            trn_error = []
            for ii in range(output.shape[0]):
                single_trn_error = []
                for iii in range(int(output.shape[1] / 3)):
                    single_trn_error.append(np.linalg.norm(output[ii, iii:iii + 3] - label[ii, iii:iii + 3]))
                trn_error.append(np.mean(single_trn_error))

            if (i_batch + 1) % 10 == 0:
                writer.add_scalar('loss/scalar1', loss.data[0], cnt)
                print(
                    'Epoch [%d/%d], Iter [%d/%d] Loss: %.4f data_time: %.4f forward_time: %.4f loss_time: %.4f backward_time: %.4f optim_time: %.4f'
                    % (i_epoch + 1, config.max_iter, i_batch + 1, len(train_data), loss.data[0], data_time, forward_time
                       , loss_time, backward_time, optim_time))
            if (i_batch + 1) % 100 == 0:
                writer.add_scalar('accuracy/scalar1', float(np.mean(trn_error)), cnt)
            if cnt % 1000 == 0:
                lr /= 3
                optimizer = torch.optim.Adam(sub_net.parameters(), lr=lr)
                # if (i_batch + 1) % 1000 == 0:
