from hourglass import HourglassNet, Bottleneck
from tensorboardX import SummaryWriter
from torch.autograd import Variable
import torch
from torchviz import make_dot

dummy_input = Variable(torch.rand(1, 3, 256, 256))

# with SummaryWriter(comment='hourglass') as w:
#     model = HourglassNet(Bottleneck, num_stacks=8, num_blocks=1, num_classes=16)
#     w.add_graph(model, (dummy_input,))
model = HourglassNet(Bottleneck, num_stacks=8, num_blocks=1, num_classes=16)
y = model.forward(dummy_input)
g = make_dot(y, params=dict(model.named_parameters()))
g.view()
