from Residual import Residual
import torch.nn as nn
import math
# import ref


class Hourglass(nn.Module):
    def __init__(self, channelIn, channelOut, layers=3):
        super(Hourglass, self).__init__()
        self.channelIn = channelIn
        self.channelOut = channelOut
        self.layers = layers

        self.para = nn.ModuleList([Residual(channelIn, 256), Residual(256, 256), Residual(256, channelOut)])
        self.maxPool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.inner = nn.ModuleList([Residual(channelIn, 256), Residual(256, 256), Residual(256, 256)])

        if layers > 1:
            self.subgraph = Hourglass(channelIn, channelOut, self.layers - 1)
        else:
            self.subgraph = Residual(256, channelOut)

        self.outInner = Residual(channelOut, channelOut)
        self.upSample = nn.Upsample(scale_factor=2)

    def forward(self, x):
        for i in range(len(self.para)):
            paraOut = self.para[i](x)
        x = self.maxPool(x)
        for i in range(len(self.inner)):
            x = self.inner[i](x)
        x = self.subgraph(x)
        x = self.outInner(x)
        x = self.upSample(x)
        return x + paraOut


class TestNet(nn.Module):
    def __init__(self, layers=3):
        super(TestNet, self).__init__()
        self.layers = layers
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU()
        self.r1 = Residual(64, 128)
        self.maxPool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.r2 = Residual(128, 128)
        self.r3 = Residual(128, 256)
        self.hg = Hourglass(256, 512)
        self.r4 = Residual(512, 256)
        self.conv2 = nn.Conv2d(256, 15, kernel_size=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.r1(x)
        x = self.maxPool(x)
        x = self.r2(x)
        x = self.r3(x)
        x = self.hg(x)
        x = self.r4(x)
        x = self.conv2(x)
        return self.maxPool(x)
