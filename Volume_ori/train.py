import torch
from network import OpenposeBasedNet
from tensorboardX import SummaryWriter
import numpy as np
from network import VSPNetwork
from data_loader import get_loader
import argparse
import time
from util import save_checkpoint
import os
import datetime

parser = argparse.ArgumentParser()
parser.add_argument('--image_size', type=int, default=256)
parser.add_argument('--stride', type=int, default=4)
parser.add_argument('--sigma', type=float, default=7)
parser.add_argument('--max_iter', type=int, default=600)
parser.add_argument('--batch_size', type=int, default=12)
parser.add_argument('--test_size', type=int, default=100)
parser.add_argument('--start_epoch', type=int, default=0)
parser.add_argument('--resume', type=str,
                    default=None)
                    # default='/media/alan/Disk/workspace/deep3dpose_pytorch/results/D3P2V_2018_04_12_20_57_17/checkpoint_52000.pth.tar')

config = parser.parse_args()

mission_name = 'D3P2V_' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

writer = SummaryWriter()

train_data = get_loader('./data', config, is_heatmap=True)
test_data = get_loader('./data', config, phase='Test', is_heatmap=True)

# best_acc = 9999999999
is_best = False

with torch.cuda.device(1):
    net = VSPNetwork().cuda()
    # criterion = module.L2Loss()
    criterion = torch.nn.MSELoss().cuda()

    lr = 0.0001
    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    if config.resume:
        if os.path.isfile(config.resume):
            print("=> loading checkpoint '{}'".format(config.resume))
            checkpoint = torch.load(config.resume)
            config.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_prec1']
            cnt = checkpoint['cnt']
            net.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(config.resume, checkpoint['epoch']))
        else:
            best_acc = 9999999999
            cnt = 0
            print("=> no checkpoint found at '{}'".format(config.resume))
    else:
        best_acc = 9999999999
        cnt = 0

    for i_epoch in range(config.max_iter):
        if i_epoch < config.start_epoch:
            continue
        total_time_start = time.time()
        tic = time.time()
        for i_batch, data in enumerate(train_data):
            # img = torch.cat([data[0][0], data[1][0]], dim=0)
            img1 = torch.autograd.Variable(data[0][0].cuda())
            img2 = torch.autograd.Variable(data[1][0].cuda())

            # label = np.asarray([data[0][1]['key_points'][i]['world_coord'].numpy() for i in range(14)])
            # label = np.transpose(label, [1, 0, 2])
            # label = torch.autograd.Variable(
            #     torch.from_numpy(np.reshape(label, [label.shape[0], label.shape[1] * label.shape[2]])).cuda())
            label = data[0][1]['world_coord_offset'].view(data[0][1]['world_coord_offset'].shape[0], -1)
            label = torch.autograd.Variable(label.cuda())

            heatmap_l_gt = torch.autograd.Variable(data[0][1]['heatmap'].permute(0, 3, 1, 2).cuda())
            heatmap_r_gt = torch.autograd.Variable(data[1][1]['heatmap'].permute(0, 3, 1, 2).cuda())

            toc = time.time()
            data_time = toc - tic

            tic = time.time()
            output, heatmap_l, heatmap_r = net.forward(img1, img2)
            toc = time.time()
            forward_time = toc - tic

            optimizer.zero_grad()

            tic = time.time()
            loss_heat_l = criterion.forward(heatmap_l, heatmap_l_gt)
            loss_heat_r = criterion.forward(heatmap_r, heatmap_r_gt)
            loss = criterion.forward(output, label)
            loss_all = loss_heat_l + loss_heat_r + loss
            toc = time.time()
            loss_time = toc - tic

            tic = time.time()
            # loss_heat_l.backward(retain_graph=True)
            # loss_heat_r.backward(retain_graph=True)
            loss_all.backward()
            toc = time.time()
            backward_time = toc - tic

            tic = time.time()
            optimizer.step()
            toc = time.time()
            optim_time = toc - tic

            cnt += 1

            output = output.data.cpu().numpy()
            label = label['world_coord_norm'].numpy()

            total_time_end = time.time()

            total_time = total_time_end - total_time_start

            trn_error = []
            for ii in range(output.shape[0]):
                single_trn_error = []
                for iii in range(int(output.shape[1] / 3)):
                    single_trn_error.append(np.linalg.norm(output[ii, iii:iii + 3] - label[ii, iii:iii + 3]))
                trn_error.append(np.mean(single_trn_error))

            if (i_batch + 1) % 10 == 0:
                writer.add_scalar('loss/scalar1', loss.data[0], cnt)
                print(
                    'Epoch [%d/%d], Iter [%d/%d] cnt: %d Loss: %.4f data_time: %.4f forward_time: %.4f loss_time: %.4f backward_time: %.4f optim_time: %.4f total_time: %.4f'
                    % (i_epoch + 1, config.max_iter, i_batch + 1, len(train_data), cnt, loss.data[0], data_time,
                       forward_time,
                       loss_time, backward_time, optim_time, total_time))
            if cnt % 100 == 0:
                writer.add_scalar('accuracy/train', float(np.mean(trn_error)), cnt)
            if cnt % 1000 == 0:
                # net.eval()
                for i_test, data_test in enumerate(test_data):
                    if i_test > config.test_size:
                        break
                    test_img1 = torch.autograd.Variable(data_test[0][0].cuda(), volatile=True)
                    test_img2 = torch.autograd.Variable(data_test[1][0].cuda(), volatile=True)
                    output_test, heatmap_l_test, heatmap_r_test = net.forward(test_img1, test_img2)
                    tst_label = np.asarray([data_test[0][1]['key_points'][i]['world_coord'].numpy() for i in range(14)])
                    tst_label = np.transpose(tst_label, [1, 0, 2])
                    tst_label = torch.autograd.Variable(
                        torch.from_numpy(
                            np.reshape(tst_label,
                                       [tst_label.shape[0], tst_label.shape[1] * tst_label.shape[2]])).cuda(),
                        volatile=True)
                    output_test = output_test.data.cpu().numpy()
                    tst_label = tst_label.data.cpu().numpy()
                    tst_error = []
                    for ii in range(output_test.shape[0]):
                        single_tst_error = []
                        for iii in range(int(output.shape[1] / 3)):
                            single_tst_error.append(
                                np.linalg.norm(output_test[ii, iii:iii + 3] - label[ii, iii:iii + 3]))
                        tst_error.append(np.mean(single_tst_error))
                    print('Testing...Iter %d/%d' % (i_test, config.test_size))
                writer.add_scalar('accuracy/test', float(np.mean(tst_error)), cnt)
                # net.train()

                if best_acc >= np.mean(tst_error):
                    best_acc = np.mean(tst_error)
                    is_best = True
                else:
                    if_best = False

                if not os.path.exists(os.path.join('./results', mission_name)):
                    os.makedirs(os.path.join('./results', mission_name), exist_ok=True)

                save_checkpoint({
                    'epoch': i_epoch + 1,
                    'state_dict': net.state_dict(),
                    'best_prec1': best_acc,
                    'optimizer': optimizer.state_dict(),
                    'cnt': cnt
                }, is_best, filename=os.path.join('./results', mission_name, 'checkpoint_%d.pth.tar' % cnt))
            # if cnt % 1000 == 0:
            #     lr /= 3
            #     optimizer = torch.optim.Adam(net.parameters(), lr=lr)
            total_time_start = time.time()
            tic = time.time()
