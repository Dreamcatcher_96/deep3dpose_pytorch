from gen_heat_pre import generate_heatmap
import os
import numpy as np
import cv2
import time

points_index = [12, 11, 20, 21, 22, 14, 15, 16, 1, 2, 3, 5, 6, 7]
path = './data'

if not os.path.exists('./data/heatmap'):
    os.makedirs('./data/heatmap', exist_ok=True)

data_list = os.listdir(path)
data_list.sort()
data = []
for (i, item) in enumerate(data_list):
    if item.split('.')[-1] == 'npy':
        data.append(np.load(os.path.join(path, item)))

for i in range(len(data)):
    for j in range(len(data[i])):
        for k in range(len(data[i][j])):
            if len(data[i][j][k]) == 0:
                # print('%d %d %d' % (i, j, k))
                continue
            data[i][j][k]['key_points'] = \
                [data[i][j][k]['key_points'][index] for index in points_index]
            for i_data in range(len(data[i][j][k]['key_points'])):
                data[i][j][k]['key_points'][i_data]['id'] = i_data + 1
            print('Loading: %d/%d %d/%d %d/%d' % (i + 1, len(data), j + 1, len(data[i]),
                                                  k + 1, len(data[i][j])))
for i in range(len(data)):
    for j in range(len(data[i])):
        for k in range(len(data[i][j])):
            img_path = data[i][j][k]['img_path']
            image = cv2.imread(img_path)
            height, width = image.shape[0:2]
            tic = time.time()
            heatmap = generate_heatmap(data[i][j][k]['key_points'], 368, 368, stride=1, sigma=7)
            toc = time.time()
            print(toc-tic)
            np.save('./data/heatmap/heatmap_%d_%d_%d.npy' % (i + 1, j + 1, k + 1), heatmap)
            data[i][j][k]['heatmap_path'] = 'heatmap_%d_%d_%d.npy' % (i + 1, j + 1, k + 1)
            print('Loading: %d/%d %d/%d %d/%d' % (i + 1, len(data), j + 1, len(data[i]),
                                                  k + 1, len(data[i][j])))

for i in range(len(data)):
    np.save('data_%d' % (i + 1), data[i])
