import torch
import torch.nn as nn
import torch.nn.init as init


def make_layers(cfg, in_channels, batch_norm=False):
    layers = []
    for layer in cfg:
        name = layer.keys()[0]
        param = layer[name]
        if param[0] == 'conv2d':
            layers.append(
                Conv2d(in_channels, out_channels=param[1], kernel_size=param[2], stride=param[3], padding=param[4],
                       relu=param[5]))
        elif param[0] == 'pool2d':
            layers.append(Pool2d(kernel_size=param[1], stride=param[2], padding=param[3], pool_type=param[4]))
        elif param[0] == 'deconv2d':
            layers.append(
                Deconv2d(in_channels, out_channels=param[1], kernel_size=param[2], stride=param[3], padding=param[4],
                         relu=param[5]))
        if not param[0] == 'pool2d':
            in_channels = param[1]
    return nn.Sequential(*layers)


class Conv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, relu=True, bn=False):
        super(Conv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding)
        self.bn = nn.BatchNorm2d(out_channels) if bn else None
        self.relu = nn.ReLU(inplace=True) if relu else None

    def forward(self, x):
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        return x


class Conv3d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, relu=True, bn=False):
        super(Conv3d, self).__init__()
        self.conv = nn.Conv3d(in_channels, out_channels, kernel_size, stride, padding)
        self.bn = nn.BatchNorm3d(out_channels) if bn else None
        self.relu = nn.ReLU(inplace=True) if relu else None

    def forward(self, x):
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        return x


class Deconv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, output_padding=0, relu=True):
        super(Deconv2d, self).__init__()
        self.deconv = nn.ConvTranspose2d(in_channels, out_channels, kernel_size, stride, padding, output_padding)
        self.relu = nn.ReLU(inplace=True) if relu else None

    def forward(self, x):
        x = self.deconv(x)
        if self.relu is not None:
            x = self.relu(x)
        return x


class Pool2d(nn.Module):
    def __init__(self, kernel_size, stride=1, padding=0, pool_type='Max'):
        super(Pool2d, self).__init__()
        if pool_type == 'Max':
            self.pool = nn.MaxPool2d(kernel_size, stride, padding)
        elif pool_type == 'Avg':
            self.pool = nn.AvgPool2d(kernel_size, stride, padding)

    def forward(self, x):
        return self.pool(x)


class Concat(nn.Module):
    def __init__(self, dim=0):
        super(Concat, self).__init__()
        self.dim = dim

    def forward(self, x, y):
        return torch.cat((x, y), self.dim)


class L2Norm(nn.Module):
    def __init__(self, n_channels, scale):
        super(L2Norm, self).__init__()
        self.n_channels = n_channels
        self.gamma = scale or None
        self.eps = 1e-10
        self.weight = nn.Parameter(torch.Tensor(self.n_channels))
        self.reset_parameters()

    def reset_parameters(self):
        init.constant(self.weight, self.gamma)

    def forward(self, x):
        norm = x.pow(2).sum(dim=1, keepdim=True).sqrt() + self.eps
        # x /= norm
        x = torch.div(x, norm)
        out = self.weight.unsqueeze(0).unsqueeze(2).unsqueeze(3).expand_as(x) * x
        return out


class L2Loss(nn.Module):
    def __init__(self):
        super(L2Loss, self).__init__()

    def forward(self, x, y):
        loss = 0
        for i in range(x.data.shape[0]):
            loss += torch.dist(x[i, :], y[i, :])
        loss /= x.data.shape[0]
        return loss


class ResModule(nn.Module):
    def __init__(self, num_in, num_out):
        super(ResModule, self).__init__()
        self.numIn = num_in
        self.numOut = num_out
        self.bn = nn.BatchNorm2d(self.numIn)
        self.relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(self.numIn, int(self.numOut / 2), bias=True, kernel_size=1)
        self.bn1 = nn.BatchNorm2d(int(self.numOut / 2))
        self.conv2 = nn.Conv2d(int(self.numOut / 2), int(self.numOut / 2), bias=True, kernel_size=3, stride=1,
                               padding=1)
        self.bn2 = nn.BatchNorm2d(int(self.numOut / 2))
        self.conv3 = nn.Conv2d(int(self.numOut / 2), self.numOut, bias=True, kernel_size=1)

        if self.numIn != self.numOut:
            self.conv4 = nn.Conv2d(self.numIn, self.numOut, bias=True, kernel_size=1)

    def forward(self, x):
        residual = x
        out = self.bn(x)
        out = self.relu(out)
        out = self.conv1(out)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)

        if self.numIn != self.numOut:
            residual = self.conv4(x)

        return out + residual


class ResModule3D(nn.Module):
    def __init__(self, num_in, num_out):
        super(ResModule3D, self).__init__()
        self.numIn = num_in
        self.numOut = num_out
        self.bn = nn.BatchNorm3d(self.numIn)
        self.relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv3d(self.numIn, int(self.numOut / 2), bias=True, kernel_size=1)
        self.bn1 = nn.BatchNorm3d(int(self.numOut / 2))
        self.conv2 = nn.Conv3d(int(self.numOut / 2), int(self.numOut / 2), bias=True, kernel_size=3, stride=1,
                               padding=1)
        self.bn2 = nn.BatchNorm3d(int(self.numOut / 2))
        self.conv3 = nn.Conv3d(int(self.numOut / 2), self.numOut, bias=True, kernel_size=1)

        if self.numIn != self.numOut:
            self.conv4 = nn.Conv3d(self.numIn, self.numOut, bias=True, kernel_size=1)

    def forward(self, x):
        residual = x
        out = self.bn(x)
        out = self.relu(out)
        out = self.conv1(out)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)

        if self.numIn != self.numOut:
            residual = self.conv4(x)

        return out + residual


class Hourglass(nn.Module):
    def __init__(self, channel_in, channel_out, layers=3):
        super(Hourglass, self).__init__()
        self.channelIn = channel_in
        self.channelOut = channel_out
        self.layers = layers

        self.encoder = []
        self.decoder = []

        self.para = nn.ModuleList([ResModule(channel_in, 256), ResModule(256, 256), ResModule(256, channel_out)])
        self.maxPool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.inner = nn.ModuleList([ResModule(channel_in, 256), ResModule(256, 256), ResModule(256, 256)])

        # if layers > 1:
        #     self.subgraph = Hourglass(channel_in, channel_out, self.layers - 1)
        # else:
        self.subgraph = ResModule(256, channel_out)

        self.outInner = ResModule(channel_out, channel_out)

        for i in range(layers):
            self.encoder.append([self.para, self.inner])
            self.decoder.append(self.outInner)

        self.upSample = nn.Upsample(scale_factor=2)

    def forward(self, x):
        # for i in range(len(self.para)):
        #     para_out = self.para[i](x)
        # x = self.maxPool(x)
        # for i in range(len(self.inner)):
        #     x = self.inner[i](x)
        # x = self.subgraph(x)
        # x = self.outInner(x)
        # x = self.upSample(x)
        inter_out = []
        para_out = []
        for i in range(self.layers):
            inter_out.append(x)
            para_out_inter = x
            for i_para in range(len(self.encoder[i][0])):
                para_out_inter = self.encoder[i][0][i_para](para_out_inter)
            para_out.append(para_out_inter)
            x = self.maxPool(x)
            for i_inner in range(len(self.encoder[i][1])):
                x = self.encoder[i][1][i_inner](x)
        x = self.subgraph(x)
        for i in range(self.layers - 1, -1, -1):
            x = self.decoder[i](x)
            x = self.upSample(x)
            x = x + para_out[i]

        return x, inter_out


class VSPVLoss(nn.Module):
    def __init__(self):
        super(VSPVLoss, self).__init__()
        self.softmax = nn.CrossEntropyLoss()
        self.l2loss = nn.MSELoss(size_average=True)

    def forward(self, conf, loc, conf_gt, loc_gt):
        losses = None
        conf_losses = None
        loc_losses = None
        for i_joint in range(14):
            conf_gt_joint = conf_gt[:, i_joint].contiguous().view(-1)
            conf_loss = self.softmax.forward(conf[:, i_joint, :], conf_gt_joint)
            index = conf_gt_joint.data.cpu().numpy()
            loc_joint = torch.cat([loc[i_batch, i_joint, :, index[i_batch]].contiguous().view(1, -1) for i_batch in range(index.shape[0])], dim=0)
            loc_loss = self.l2loss.forward(loc_joint, loc_gt[:, i_joint, :])
            loss = conf_loss + loc_loss
            if losses is None:
                losses = loss
                conf_losses = conf_loss
                loc_losses = loc_loss
            else:
                losses += loss
                conf_losses += conf_loss
                loc_losses += loc_loss
        losses /= 14
        conf_losses /= 14
        loc_losses /= 14
        return losses, conf_losses, loc_losses
