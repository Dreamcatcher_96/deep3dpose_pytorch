from torchvision import transforms
import cv2
from PIL import Image
import numpy as np

img = Image.open('/media/alan/Data/Human3.6m/S1/Images/c2/Directions_1_c2_1.jpg')

width, height = img.size

augment = transforms.ColorJitter(brightness=0.25, contrast=0.25, saturation=0.25, hue=0.1)

for i in range(100):
    img_show = img.crop((300, 300, width-300, height-300))
    img_show = augment(img_show)
    img_show = np.array(img_show)

    cv2.imshow('res', img_show)
    cv2.waitKey(100)
