import torch
import shutil
import os
import numpy as np


def save_checkpoint(state, is_best, filename='./checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.join(*(filename.split('/')[:-1] + ['model_best.pth.tar'])))


def decode_volume(output_conf, output_location, dims, bbox_3d):
    for ii_batch in range(output_conf.shape[0]):
        for i_joint in range(14):
            loc_x = np.floor(output_conf[ii_batch, i_joint] / 64)
            loc_y = np.floor(output_conf[ii_batch, i_joint] % 64 / 8)
            loc_z = np.floor(output_conf[ii_batch, i_joint] % 64 % 8)
            loc_x *= 0.125
            loc_y *= 0.125
            loc_z *= 0.125
            output_location[ii_batch, i_joint, 0] += loc_x
            output_location[ii_batch, i_joint, 1] += loc_y
            output_location[ii_batch, i_joint, 2] += loc_z
            output_location[ii_batch, i_joint, :] = \
                output_location[ii_batch, i_joint, :] * dims[ii_batch, :] + bbox_3d[0][ii_batch, :]
    return output_location
