import numpy as np
import time
from PIL import Image
from torchvision import transforms
import argparse
from data_loader import get_loader
import cv2

parser = argparse.ArgumentParser()
parser.add_argument('--image_size', type=int, default=368)
parser.add_argument('--stride', type=int, default=8)
parser.add_argument('--sigma', type=float, default=7)
parser.add_argument('--batch_size', type=float, default=10)

config = parser.parse_args()

loader = get_loader('./data', config, test_index=[1, 2, 3, 4, 5, 6])

for epoch in range(2):
    for i_batch, data in enumerate(loader):
        if epoch == 0:
            print(i_batch)
            continue
        coord = [data[0][1]['key_points'][i]['coord'].numpy()[0, :] for i in range(14)]
        img_raw = cv2.imread(data[0][1]['img_path'][0])
        img = data[0][0][0, :, :, :]
        img = (img.permute(1, 2, 0).numpy() + 0.5) * 256
        img = img.astype(np.uint8)
        for i in range(len(coord)):
            img[int(coord[i][1]-1):int(coord[i][1]+1), int(coord[i][0]-1):int(coord[i][0]+1), :] = (0, 255, 0)
            # cv2.circle(img, center=(coord[i][0], coord[i][1]), radius=1, color=(0, 255, 0))
        cv2.imshow('res', img)
        cv2.waitKey()

print(config)
