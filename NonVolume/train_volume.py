import torch
from network import OpenposeBasedNet
from tensorboardX import SummaryWriter
import numpy as np
from network import VSPNetworkV
from data_loader import get_loader
import argparse
import time
import module
from util import save_checkpoint
from util import decode_volume
import os
import datetime
import socket

parser = argparse.ArgumentParser()
parser.add_argument('--image_size', type=int, default=256)
parser.add_argument('--stride', type=int, default=4)
parser.add_argument('--sigma', type=float, default=7)
parser.add_argument('--max_iter', type=int, default=600)
parser.add_argument('--batch_size', type=int, default=10)
parser.add_argument('--test_size', type=int, default=100)
parser.add_argument('--start_epoch', type=int, default=0)
parser.add_argument('--resume', type=str,
                    default=None)
# default='/home/alan/workspace/deep3dpose_pytorch/results/D3P2V_Volume_2018_04_20_11_32_09/checkpoint_32000.pth.tar')

config = parser.parse_args()

mission_name = 'D3P2V_Volume_' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

current_time = datetime.datetime.now().strftime('%b%d_%H-%M-%S')
log_dir = os.path.join('../runs', current_time + '_' + socket.gethostname())
writer = SummaryWriter(log_dir=log_dir)

train_data = get_loader('../data', config, is_heatmap=True, is_volume=True)
test_data = get_loader('../data', config, phase='Test', is_heatmap=True, is_volume=True)

# best_acc = 9999999999
is_best = False

with torch.cuda.device(1):
    net = VSPNetworkV().cuda()
    # criterion = module.L2Loss()
    criterion = torch.nn.MSELoss().cuda()
    VSPLoss = module.VSPVLoss().cuda()

    lr = 0.0001
    optimizer = torch.optim.Adam(net.parameters(), lr=lr)

    if config.resume:
        if os.path.isfile(config.resume):
            print("=> loading checkpoint '{}'".format(config.resume))
            checkpoint = torch.load(config.resume)
            config.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_prec1']
            cnt = checkpoint['cnt']
            lr = checkpoint['lr']
            net.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(config.resume, checkpoint['epoch']))
        else:
            best_acc = 9999999999
            cnt = 0
            print("=> no checkpoint found at '{}'".format(config.resume))
    else:
        best_acc = 9999999999
        cnt = 0

    for i_epoch in range(config.max_iter):
        if i_epoch < config.start_epoch:
            continue
        total_time_start = time.time()
        tic = time.time()
        for i_batch, data in enumerate(train_data):
            # img = torch.cat([data[0][0], data[1][0]], dim=0)
            img1 = torch.autograd.Variable(data[0][0].cuda())
            img2 = torch.autograd.Variable(data[1][0].cuda())

            # label = np.asarray([data[0][1]['key_points'][i]['world_coord'].numpy() for i in range(14)])
            # label = np.transpose(label, [1, 0, 2])
            # label = torch.autograd.Variable(
            #     torch.from_numpy(np.reshape(label, [label.shape[0], label.shape[1] * label.shape[2]])).cuda())
            label_class = torch.autograd.Variable(data[0][1]['volume_class'].cuda())
            label_loc = torch.autograd.Variable(data[0][1]['volume_loc'].cuda())
            # label_yloc = torch.autograd.Variable(data[0][1]['volume_loc_y'].cuda())
            # label_zloc = torch.autograd.Variable(data[0][1]['volume_loc_z'].cuda())
            # label = torch.autograd.Variable(label.cuda())

            heatmap_l_gt = torch.autograd.Variable(data[0][1]['heatmap'].permute(0, 3, 1, 2).cuda())
            heatmap_r_gt = torch.autograd.Variable(data[1][1]['heatmap'].permute(0, 3, 1, 2).cuda())

            toc = time.time()
            data_time = toc - tic

            tic = time.time()
            output_classify, output_loc, heatmap_l, heatmap_r = net.forward(img1, img2)
            toc = time.time()
            forward_time = toc - tic

            optimizer.zero_grad()

            tic = time.time()
            loss_heat_l = criterion.forward(heatmap_l, heatmap_l_gt)
            loss_heat_r = criterion.forward(heatmap_r, heatmap_r_gt)
            loss, conf_loss, loc_loss = VSPLoss.forward(output_classify, output_loc,
                                                        label_class.long(), label_loc.float())
            loss_all = loss_heat_l + loss_heat_r + loss
            toc = time.time()
            loss_time = toc - tic

            tic = time.time()
            # loss_heat_l.backward(retain_graph=True)
            # loss_heat_r.backward(retain_graph=True)
            loss_all.backward()
            toc = time.time()
            backward_time = toc - tic

            tic = time.time()
            optimizer.step()
            toc = time.time()
            optim_time = toc - tic

            cnt += 1

            output_conf = torch.nn.functional.softmax(output_classify, dim=2).data.cpu().numpy()
            output_conf = np.argmax(output_conf, axis=2)
            conf_gt = data[0][1]['volume_class'].numpy()
            conf_acc_trn = np.sum(output_conf == conf_gt) / output_conf.size * 100
            # output_location_x = output_loc_x.data.cpu().numpy()
            # output_location_y = output_loc_y.data.cpu().numpy()
            # output_location_z = output_loc_z.data.cpu().numpy()
            output_location = []
            for i_joint in range(14):
                output_location_single = []
                for ii_batch in range(output_conf.shape[0]):
                    output_location_single.append(output_loc[ii_batch, i_joint, :, output_conf[ii_batch, i_joint]])
                output_location.append(
                    torch.cat(output_location_single, dim=0).view(len(output_location_single), -1, 3))
            output_location = torch.cat(output_location, dim=1).data.cpu().numpy()
            loc_gt = data[0][1]['volume_loc'].numpy()

            dims = data[0][1]['dim'].numpy()

            loc_error_trn = []
            for ii in range(output_location.shape[0]):
                single_trn_error = []
                for iii in range(int(output_location.shape[1])):
                    single_trn_error.append(
                        np.linalg.norm((output_location[ii, iii, :] - loc_gt[ii, iii, :]) * dims[ii, :]))
                    loc_error_trn.append(np.mean(single_trn_error))

            bbox_3d = [data[0][1]['3dbox_coord'][0].numpy(), data[0][1]['3dbox_coord'][1].numpy()]
            world_coord_gt = data[0][1]['world_coord'].numpy()

            output_location = decode_volume(output_conf, output_location, dims, bbox_3d)

            total_time_end = time.time()

            total_time = total_time_end - total_time_start

            trn_error = []
            for ii in range(output_location.shape[0]):
                single_trn_error = []
                for iii in range(int(output_location.shape[1])):
                    single_trn_error.append(np.linalg.norm(output_location[ii, iii, :] - world_coord_gt[ii, iii, :]))
                trn_error.append(np.mean(single_trn_error))

            if (i_batch + 1) % 10 == 0:
                writer.add_scalar('loss/all_loss', loss_all.data[0], cnt)
                writer.add_scalar('loss/loss', loss.data[0], cnt)
                writer.add_scalar('loss/conf_loss', conf_loss.data[0], cnt)
                writer.add_scalar('loss/loc_loss', loc_loss.data[0], cnt)
                writer.add_scalar('learning/lr', lr, cnt)
            print(
                'Epoch [%d/%d], Iter [%d/%d] cnt: %d Loss: %.4f data_time: %.4f forward_time: %.4f loss_time: %.4f backward_time: %.4f optim_time: %.4f total_time: %.4f'
                % (i_epoch + 1, config.max_iter, i_batch + 1, len(train_data), cnt, loss_all.data[0], data_time,
                   forward_time,
                   loss_time, backward_time, optim_time, total_time))
            if cnt % 10 == 0:
                writer.add_scalar('accuracy/train', float(np.mean(trn_error)), cnt)
                writer.add_scalar('accuracy/conf_acc_trn', float(conf_acc_trn), cnt)
                writer.add_scalar('accuracy/loc_err_trn', float(np.mean(loc_error_trn)), cnt)

            if cnt % 2000 == 0:
                net.eval()
                for i_test, data_test in enumerate(test_data):
                    if i_test > config.test_size:
                        break
                    test_img1 = torch.autograd.Variable(data_test[0][0].cuda(), volatile=True)
                    test_img2 = torch.autograd.Variable(data_test[1][0].cuda(), volatile=True)
                    output = net.forward(test_img1, test_img2)
                    dims_tst = data_test[0][1]['dim'].numpy()
                    bbox_3d_tst = [data_test[0][1]['3dbox_coord'][0].numpy(), data_test[0][1]['3dbox_coord'][1].numpy()]
                    world_coord_gt_tst = data_test[0][1]['world_coord'].numpy()

                    conf_tst = torch.nn.functional.softmax(output[0], dim=2).data.cpu().numpy()
                    conf_tst = np.argmax(conf_tst, axis=2)
                    conf_tst_gt = data_test[0][1]['volume_class'].numpy()

                    conf_acc_tst = np.sum(conf_tst == conf_tst_gt) / conf_tst.size * 100

                    loc_tst_gt = data_test[0][1]['volume_loc'].numpy()

                    loc_tst = []
                    for i_joint in range(14):
                        loc_tst_single = []
                        for ii_batch in range(conf_tst.shape[0]):
                            loc_tst_single.append(
                                output[1][ii_batch, i_joint, :, conf_tst[ii_batch, i_joint]].contiguous().view(1, -1))
                        loc_tst.append(
                            torch.cat(loc_tst_single, dim=0).view(len(loc_tst_single), -1, 3))
                    loc_tst = torch.cat(loc_tst, dim=1).data.cpu().numpy()

                    loc_err_tst = []
                    for ii in range(loc_tst.shape[0]):
                        single_tst_error = []
                        for iii in range(int(loc_tst.shape[1])):
                            single_tst_error.append(
                                np.linalg.norm((loc_tst[ii, iii, :] - loc_tst_gt[ii, iii, :]) * dims_tst[ii, :]))
                            loc_err_tst.append(np.mean(single_tst_error))

                    loc_tst = decode_volume(conf_tst, loc_tst, dims_tst, bbox_3d_tst)

                    tst_error = []
                    for ii in range(loc_tst.shape[0]):
                        single_tst_error = []
                        for iii in range(int(loc_tst.shape[1])):
                            single_tst_error.append(
                                np.linalg.norm(loc_tst[ii, iii, :] - world_coord_gt_tst[ii, iii, :]))
                        tst_error.append(np.mean(single_tst_error))
                    print('Testing...Iter %d/%d' % (i_test, config.test_size))
                writer.add_scalar('accuracy/test', float(np.mean(tst_error)), cnt)
                writer.add_scalar('accuracy/conf_acc_tst', float(conf_acc_tst), cnt)
                writer.add_scalar('accuracy/loc_err_tst', float(np.mean(loc_err_tst)), cnt)
                net.train()

                if best_acc >= np.mean(tst_error):
                    best_acc = np.mean(tst_error)
                    is_best = True
                else:
                    if_best = False

                if not os.path.exists(os.path.join('./results', mission_name)):
                    os.makedirs(os.path.join('./results', mission_name), exist_ok=True)

                save_checkpoint({
                    'epoch': i_epoch + 1,
                    'state_dict': net.state_dict(),
                    'best_prec1': best_acc,
                    'optimizer': optimizer.state_dict(),
                    'cnt': cnt,
                    'lr': lr
                }, is_best, filename=os.path.join('./results', mission_name, 'checkpoint_%d.pth.tar' % cnt))

            if cnt % 5000 == 0:
                lr *= 0.9
                optimizer = torch.optim.Adam(net.parameters(), lr=lr)
            total_time_start = time.time()
            tic = time.time()
