import torch
import shutil
import os


def save_checkpoint(state, is_best, filename='./checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, os.path.join(*(filename.split('/')[:-1] + ['model_best.pth.tar'])))
