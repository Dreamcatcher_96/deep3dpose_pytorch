import numpy as np
from libc.math cimport exp

def generate_heatmap(keypoints, int image_height, int image_width, int stride, float sigma):
    cdef int x,y,xx,yy
    cdef float dis

    heatmap = np.zeros((int(image_height / stride), int(image_width / stride), len(keypoints)), dtype=np.float32)
    height, width = heatmap.shape[0:2]
    start = stride / 2 - 0.5

    for i in range(len(keypoints)):
        if keypoints[i]['id'] == -1:
            continue
        x = int(keypoints[i]['coord'][0])
        y = int(keypoints[i]['coord'][1])
        for h in range(height):
            for w in range(width):
                xx = start + w * stride
                yy = start + h * stride
                dis = ((xx - x) * (xx - x) + (yy - y) * (yy - y)) / 2.0 / sigma / sigma
                if dis > 4.6052:
                    continue
                heatmap[h][w][i] += exp(-dis)
                if heatmap[h][w][i] > 1:
                    heatmap[h][w][i] = 1
    return heatmap