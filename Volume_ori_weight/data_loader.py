import os
import numpy as np
import time
import torch
from torch.utils import data
import cv2
from torchvision import transforms
import math
from PIL import Image
import copy
from generate_heatmap import generate_heatmap


# def generate_heatmap(keypoints, image_size, stride, sigma):
#     heatmap = np.zeros((int(image_size / stride), int(image_size / stride), len(keypoints) + 1), dtype=np.float32)
#     height, width = heatmap.shape[0:2]
#     start = stride / 2 - 0.5
#
#     for i in range(len(keypoints)):
#         if keypoints[i] is None:
#             continue
#         x = keypoints[i]['coord'][0]
#         y = keypoints[i]['coord'][1]
#         for h in range(height):
#             for w in range(width):
#                 xx = start + w * stride
#                 yy = start + h * stride
#                 dis = ((xx - x) ** 2 + (yy - y) ** 2) / 2.0 / sigma / sigma
#                 if dis > 4.6052:
#                     continue
#                 heatmap[h][w][i] += math.exp(-dis)
#                 if heatmap[h][w][i] > 1:
#                     heatmap[h][w][i] = 1
#     return heatmap


class DataPreprocess(object):
    def __init__(self, config):
        self.config = config

    def __call__(self, image, label, img_size, is_heatmap=True, is_volume=True):
        bbox = label['bbox'].flatten()
        keypoints = label['key_points']
        keypoints_coord = np.asarray([keypoints[index]['coord'] for index in range(len(keypoints))])
        kpt_outline = np.asarray([np.min(keypoints_coord, axis=0), np.max(keypoints_coord, axis=0)]).flatten()
        bbox[0:2] = [min(kpt_outline[i], bbox[i]) for i in [0, 1]]
        bbox[2:4] = [max(kpt_outline[i], bbox[i]) for i in [2, 3]]
        bbox_w, bbox_h = (bbox[2] - bbox[0], bbox[3] - bbox[1])

        bbox[0] = bbox[0] - bbox_w * 0.025
        bbox[1] = bbox[1] - bbox_h * 0.025
        bbox[2] = bbox[2] + bbox_w * 0.025
        bbox[3] = bbox[3] + bbox_h * 0.025

        bbox = np.floor(bbox)
        diff = bbox[2] - bbox[0] - bbox[3] + bbox[1]
        if diff > 0:
            bbox[1] -= diff / 2
            bbox[3] += diff / 2
        elif diff < 0:
            diff = -diff
            bbox[0] -= diff / 2
            bbox[2] += diff / 2
        bbox = [int(bbox[i]) for i in range(len(bbox))]

        bbox[0] = max(0, min(bbox[0], image.size[1]))
        bbox[1] = max(0, min(bbox[1], image.size[0]))
        bbox[2] = max(0, min(bbox[2], image.size[1]))
        bbox[3] = max(0, min(bbox[3], image.size[0]))

        abs_diff = abs((bbox[2] - bbox[0]) - (bbox[3] - bbox[1]))
        if abs_diff != 0:
            if bbox[0] == 0:
                bbox[2] += abs_diff
            if bbox[1] == 0:
                bbox[3] += abs_diff
            if bbox[2] == image.size[1]:
                bbox[0] -= abs_diff
            if bbox[3] == image.size[0]:
                bbox[1] -= abs_diff

        image = image.crop((bbox[0], bbox[1], bbox[2], bbox[3]))
        # image = image[bbox[1]:bbox[3], bbox[0]:bbox[2], :]
        # image = cv2.resize(image, (img_size, img_size))
        # image = (image.astype(np.float32) / 256) - 0.5

        label['crop_coord'] = bbox
        if (bbox[2] - bbox[0]) != (bbox[3] - bbox[1]):
            print('debug')
        crop_size = bbox[2] - bbox[0]
        for i in range(len(label['key_points'])):
            if label['key_points'][i] is None:
                continue
            label['key_points'][i]['coord'][0] = (label['key_points'][i]['coord'][0] - bbox[0]) / \
                                                 crop_size * self.config.image_size
            label['key_points'][i]['coord'][1] = (label['key_points'][i]['coord'][1] - bbox[1]) / \
                                                 crop_size * self.config.image_size
            if (label['key_points'][i]['coord'][0] > image.size[1] or label['key_points'][i]['coord'][0] < 0 or
                        label['key_points'][i]['coord'][1] > image.size[0] or label['key_points'][i]['coord'][1] < 0):
                label['key_points'][i]['id'] = -1

        world_coord = np.asarray(
            [label['key_points'][i_kpt]['world_coord'] for i_kpt in range(len(label['key_points']))])
        center_point = np.mean(world_coord, axis=0)
        world_coord_offset = np.asarray(
            [world_coord[i_kpt, :] - center_point for i_kpt in range(len(label['key_points']))])

        label['world_coord'] = world_coord
        label['center_point'] = center_point
        label['world_coord_offset'] = world_coord_offset

        if is_volume:
            # volume_loc_y = np.zeros([4, 4, 4])
            # volume_loc_z = np.zeros([4, 4, 4])
            max_xyz = np.max(world_coord, axis=0)
            min_xyz = np.min(world_coord, axis=0)
            label['3dbox_coord'] = [min_xyz - (max_xyz - min_xyz) * 0.05, max_xyz + (max_xyz - min_xyz) * 0.05]
            label['dim'] = np.asarray(label['3dbox_coord'][1] - label['3dbox_coord'][0])
            # label['dim'] = np.asarray([2030] * 3)
            # label['3dbox_coord'] = [label['3dbox_coord'][0] - (label['dim'] - (label['3dbox_coord'][1] - label['3dbox_coord'][0])) / 2,
            #                         label['3dbox_coord'][1] + (label['dim'] - (label['3dbox_coord'][1] - label['3dbox_coord'][0])) / 2]
            world_norm = (world_coord - label['3dbox_coord'][0]) / label['dim']
            label['world_coord_norm'] = world_norm
            xyz = np.floor(world_norm / 0.25).astype(np.int32)
            xyz_loc = (world_norm - xyz * 0.25) #* label['dim']
            # volume_class = xyz[:, 0] * 16 + xyz[:, 1] * 4 + xyz[:, 2]
            label['volume_class'] = xyz[:, 0] * 16 + xyz[:, 1] * 4 + xyz[:, 2]
            label['volume_loc'] = xyz_loc
            # label['volume_loc_y'] = volume_loc_y
            # label['volume_loc_z'] = volume_loc_z

        if is_heatmap:
            label['heatmap'] = generate_heatmap(label['key_points'],
                                                self.config.image_size, self.config.stride, self.config.sigma)
        return image, label


class H36MDataset(data.Dataset):
    def __init__(self, path, transfrom, config, test_index=(3, 4), phase='Train', is_heatmap=True, is_volume=True):
        self.data = []
        self.config = config
        self.is_heatmap = is_heatmap
        self.is_volume = is_volume
        self.phase = phase
        points_index = [12, 11, 20, 21, 22, 14, 15, 16, 1, 2, 3, 5, 6, 7]

        data_list = os.listdir(path)
        data_list.sort()
        for (i, item) in enumerate(data_list):
            if item.split('.')[-1] == 'npy':
                if phase == 'Test' and i not in test_index:
                    continue
                elif phase == 'Train' and i in test_index:
                    continue
                self.data.append(np.load(os.path.join(path, item)))
        self.transform = transfrom
        self.index_map = []
        for i in range(len(self.data)):
            for j in range(len(self.data[i])):
                self.index_map.append((i, j, 0))
                add_flag = True
                for k in range(len(self.data[i][j])):
                    if len(self.data[i][j][k]) == 0:
                        # print('%d %d %d' % (i, j, k))
                        add_flag = False
                        continue
                    self.data[i][j][k]['key_points'] = \
                        [self.data[i][j][k]['key_points'][index] for index in points_index]
                    for i_data in range(len(self.data[i][j][k]['key_points'])):
                        self.data[i][j][k]['key_points'][i_data]['id'] = i_data + 1
                    print('Loading: %d/%d %d/%d %d/%d' % (i + 1, len(self.data), j + 1, len(self.data[i]),
                                                          k + 1, len(self.data[i][j])))
                if add_flag:
                    self.index_map.append((i, j, 1))

    def __len__(self):
        return len(self.index_map)

    def __getitem__(self, index):
        i_person, i_frame, i_cam_pair = self.index_map[index]
        train_pair = []
        cam_paris = [[1, 3], [0, 2]]
        for i_cam in cam_paris[i_cam_pair]:
            data_record = copy.deepcopy(self.data[i_person][i_frame][i_cam])
            img_path = data_record['img_path']
            # image = cv2.imread(img_path)
            image = Image.open(img_path)

            if self.phase == 'Train':
                data_aug = transforms.Compose([transforms.Resize([self.config.image_size, self.config.image_size]),
                                               transforms.ColorJitter(brightness=0.25, contrast=0.25, saturation=0.25,
                                                                      hue=0.1),
                                               transforms.ToTensor()])
            else:
                data_aug = transforms.Compose([transforms.Resize([self.config.image_size, self.config.image_size]),
                                               transforms.ToTensor()])
            if self.transform is not None:
                image, data_record = self.transform(image, data_record, self.config.image_size,
                                                    is_heatmap=self.is_heatmap, is_volume=self.is_volume)
                image = data_aug(image)
                # image = torch.from_numpy(image)
                # image = image.transpose(0, 1).transpose(0, 2).contiguous()
                # image = data_aug(image)
            train_pair.append((image, data_record))
        return train_pair


def get_loader(data_path, config, test_index=(3, 4), phase='Train', shuffle=True, is_heatmap=True, is_volume=True,
               num_workers=4):
    transform = DataPreprocess(config)

    dataset = H36MDataset(data_path, transform, config, test_index, phase, is_heatmap=is_heatmap, is_volume=is_volume)

    data_loader = data.DataLoader(dataset=dataset, batch_size=config.batch_size, shuffle=shuffle,
                                  num_workers=num_workers)
    return data_loader
