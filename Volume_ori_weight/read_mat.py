import scipy.io
import numpy as np
import os

dataset = scipy.io.loadmat('/home/alan/workspace/deep3dpose/data/data_h36m.mat')['dataset']

# base_dir = '/media/alan/Data/Human3.6m'
base_dir = '/home/hanxi/Desktop/Human3.6m/H3MData'

data = []
for i in range(dataset.shape[1]):
    data_person = []

    for i_frame in range(dataset[0, i].shape[1]):
        data_frame = []
        for i_cam in range(dataset[0, i].shape[0]):
            if len(dataset[0, i][i_cam, i_frame]) == 0:
                data_frame.append([])
                continue
            data_per_cam = {
                'img_path': os.path.join(base_dir, *dataset[0, i][i_cam, i_frame]['img_path'][0, 0][0].split('/')[5:]),
                'type': dataset[0, i][i_cam, i_frame]['type'][0, 0][0],
                'cam_no': int(dataset[0, i][i_cam, i_frame]['cam_no'][0, 0][0, 0]),
                'frame_no': int(dataset[0, i][i_cam, i_frame]['frame_no'][0, 0][0, 0]),
                'bbox': dataset[0, i][i_cam, i_frame]['bbox'][0, 0] - 1}
            key_points = {'name': [],
                          'id': [],
                          'coord': [],
                          'coord_3d': [],
                          'world_coord': []}
            for i_point in range(dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0].shape[0]):
                # point_attr = {'name': dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['name'][0],
                #               'id': int(dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['id'][0][0]),
                #               'coord': dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['coord'][0] - 1,
                #               'coord_3d': dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['coord_3d'][0],
                #               'world_coord':
                #                   dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['world_coord'][0]}

                key_points['name'].append(dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['name'][0])
                key_points['id'].append(int(dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['id'][0][0]))
                key_points['coord'].append(
                    dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['coord'][0] - 1)
                key_points['coord_3d'].append(
                    dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['coord_3d'][0])
                key_points['world_coord'].append(
                    dataset[0, i][i_cam, i_frame]['key_points'][0, 0][0][i_point]['world_coord'][0])
            data_per_cam['key_points'] = key_points
            CameraMatrix = {
                'IntrinsicMatrix': dataset[0, i][i_cam, i_frame]['CameraMatrix'][0, 0][0]['IntrinsicMatrix'][0],
                'Rotation': dataset[0, i][i_cam, i_frame]['CameraMatrix'][0, 0][0]['Rotation'][0],
                'Translation': dataset[0, i][i_cam, i_frame]['CameraMatrix'][0, 0][0]['Translation'][0],
                'k': dataset[0, i][i_cam, i_frame]['CameraMatrix'][0, 0][0]['k'][0],
                'p': dataset[0, i][i_cam, i_frame]['CameraMatrix'][0, 0][0]['p'][0]}
            data_per_cam['CameraMatrix'] = CameraMatrix
            data_frame.append(data_per_cam)
            print('%d/%d %d/%d %d/%d' % (
                i + 1, dataset.shape[1], i_frame + 1, dataset[0, i].shape[1], i_cam + 1, dataset[0, i].shape[0]))
        data_person.append(data_frame)
    np.save('./data/data_%d' % (i + 1), data_person)
